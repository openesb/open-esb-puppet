class postgredeb::install {
  if ($::environment =='db' or ($::environment =='cluster' and $::hostname==('postgresql'))) {
    exec { 'postgresql-9.3.deb':
      command => "/usr/bin/wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | /usr/bin/sudo /usr/bin/apt-key add -; /usr/bin/sudo sh -c '/bin/echo \"deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main\" >> /etc/apt/sources.list.d/postgresql.list';/usr/bin/sudo /usr/bin/apt-get update",
      before  => Package['postgresql-9.3'],
    }
  }
}