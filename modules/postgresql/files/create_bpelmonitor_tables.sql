CREATE TABLE  esb_bpelse_owner.SERVICEUNIT (
    suname varchar(256),
    suziparchive bytea NOT NULL,
    lastupdatetime TIMESTAMP,
    primary key (suname)
);
ALTER TABLE  esb_bpelse_owner.SERVICEUNIT
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.MONITORBPELPROCESS (
    suname varchar(256),
    bpelid varchar(1028),
    genbpelevents char(1),
    primary key (bpelid)
);
ALTER TABLE  esb_bpelse_owner.MONITORBPELPROCESS
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.MONITORBPELINSTANCE (
    engineid varchar(128),
    instanceid varchar(128),
    bpelid varchar(1028),
    status varchar(32) DEFAULT 'RUNNING',  
    starttime TIMESTAMP,
    endtime TIMESTAMP,
    updatedtime TIMESTAMP,    
    primary key (instanceid)
);
ALTER TABLE  esb_bpelse_owner.MONITORBPELINSTANCE
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.MONITORBPELACTIVITY (
    engineid varchar(128),
    instanceid varchar(128),
    activityid numeric(16,0),
    activityxpath varchar (2000),
    iteration  numeric(4,0) DEFAULT 0,
    status varchar(32) DEFAULT 'STARTED',
    hasfaulted char(1) DEFAULT 'N',
    crmpreceiveid varchar(128),    
  crmpinvokeid varchar(128),      
    starttime TIMESTAMP,
    endtime TIMESTAMP,
    primary key (instanceid, activityid, iteration)
);
ALTER TABLE  esb_bpelse_owner.MONITORBPELACTIVITY
  OWNER TO esb_bpelse_owner;
  
CREATE TABLE  esb_bpelse_owner.MONITORBPELVARIABLE (
    instanceid varchar(128),
    scopeid numeric(16,0),
    varid numeric(16,0),
    varname varchar (255),
  isfault char(1) DEFAULT 'N',
    varvalue text,
    primary key (instanceid, scopeid, varname)
);
ALTER TABLE  esb_bpelse_owner.MONITORBPELVARIABLE
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.MONITORSIMPLEVARIABLE (
  instanceid varchar(128),
  scopeid numeric(16,0),
  varid numeric(16,0),
  varType char(1),
  varName varchar (255),
  strvalue varchar (4000),
  numvalue numeric(20,5),
  datevalue TIMESTAMP,
  primary key (instanceid, scopeid, varName)
);
ALTER TABLE  esb_bpelse_owner.MONITORSIMPLEVARIABLE
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.MONITORNMPROPERTY (
  instanceid varchar(128),
  scopeid numeric(16,0),
  varid numeric(16,0),
  propname varchar(1028),
  propvalue text,
  primary key (instanceid, scopeid, varid, propname)
);
ALTER TABLE  esb_bpelse_owner.MONITORNMPROPERTY
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.MONITORNMSIMPLEPROPERTY (
  instanceid varchar(128),
  scopeid numeric(16,0),
  varid numeric(16,0),
  propname varchar(1028),
  propvalue varchar(4000),
  primary key (instanceid, scopeid, varid, propname)
);
ALTER TABLE  esb_bpelse_owner.MONITORNMSIMPLEPROPERTY
  OWNER TO esb_bpelse_owner;

CREATE TABLE   esb_bpelse_owner.MONITORVARIABLEATTACHMENT (
  instanceid varchar(128),
  scopeid numeric(16,0),
  varid numeric(16,0),
  name varchar(1028),
  attachment bytea,
  primary key (instanceid, scopeid, varid, name)
);
ALTER TABLE esb_bpelse_owner.MONITORVARIABLEATTACHMENT
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.MONITORBPELACTIVITYVARIABLE (
    instanceid varchar(128),
    varid numeric(16,0),
    activityid numeric(16,0),
    varname varchar(255),
    isfault char(1) DEFAULT 'N',
    varvalue text,
    vardatatype  varchar(32) DEFAULT 'STRING',
    vartype varchar(32),
    primary key (instanceid, activityid, varname, vartype)
);
ALTER TABLE  esb_bpelse_owner.MONITORBPELACTIVITYVARIABLE
  OWNER TO esb_bpelse_owner;