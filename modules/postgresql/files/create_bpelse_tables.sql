CREATE TABLE   esb_bpelse_owner.ENGINE (
    engineid varchar(128),
    location varchar(1028),
    -- expiration number,
    lastupdatetime timestamp,
    primary key (engineid)
) ;
ALTER TABLE  esb_bpelse_owner.ENGINE
  OWNER TO esb_bpelse_owner;
CREATE INDEX  ENGINELASTUT on  esb_bpelse_owner.ENGINE (lastupdatetime);

CREATE TABLE  esb_bpelse_owner.STATE (
    stateid varchar(128),
    bpelid varchar(1028),
    engineid varchar(128),
    ownerlock char(1) DEFAULT 'Y',
    status varchar(32) DEFAULT 'RUNNING',
    primary key (stateid)
) ;
ALTER TABLE  esb_bpelse_owner.STATE
  OWNER TO esb_bpelse_owner;
CREATE INDEX  STATESTATUSINDEX on  esb_bpelse_owner.STATE (status);
CREATE INDEX  STATEENGINEIDINDEX on  esb_bpelse_owner.STATE (engineid); 

CREATE TABLE esb_bpelse_owner.WAITINGIMA (
    stateid varchar(128),
    partnerlink varchar(1028),
    operation varchar(1028),
    foreign key (stateid) references esb_bpelse_owner.STATE (stateid)
);
ALTER TABLE esb_bpelse_owner.WAITINGIMA
  OWNER TO esb_bpelse_owner;
CREATE INDEX  WAITINGIMAINDEX on esb_bpelse_owner.WAITINGIMA (stateid, partnerlink, operation);

CREATE TABLE  esb_bpelse_owner.VARIABLE (
    stateid varchar(128),
    varid decimal,
    scalabilitypassivated char(1) DEFAULT 'N',
    value text,
    scopeguid varchar(128),
    primary key (stateid, varid, scalabilitypassivated, scopeguid)
) ;
ALTER TABLE  esb_bpelse_owner.VARIABLE
  OWNER TO esb_bpelse_owner;
CREATE INDEX  VARIABLEFKEY on  esb_bpelse_owner.VARIABLE (stateid);

CREATE TABLE  esb_bpelse_owner.NMPROPERTY (
  stateid varchar(128),
  scopeguid varchar(128),
  varid decimal,
  propname varchar(1028),
  propvalue text,
  primary key (stateid, scopeguid, varid, propname)
) ;
ALTER TABLE  esb_bpelse_owner.NMPROPERTY
  OWNER TO esb_bpelse_owner;
CREATE INDEX  NMPROPERTYFKEY on  esb_bpelse_owner.NMPROPERTY (stateid);

CREATE TABLE  esb_bpelse_owner.NMSIMPLEPROPERTY (
  stateid varchar(128),
  scopeguid varchar(128),
  varid decimal,
  propname varchar(1028),
  propvalue varchar(4000),
  primary key (stateid, scopeguid, varid, propname)
) ;
ALTER TABLE  esb_bpelse_owner.NMSIMPLEPROPERTY
  OWNER TO esb_bpelse_owner;
CREATE INDEX  NMSIMPLEPROPERTYFKEY on  esb_bpelse_owner.NMSIMPLEPROPERTY (stateid);

CREATE TABLE  esb_bpelse_owner.VARIABLEATTACHMENT (
  stateid varchar(128),
  scopeguid varchar(128),
  varid decimal,
  name varchar(1028),
  attachment text,
  primary key (stateid, scopeguid, varid, name)
) ;
ALTER TABLE  esb_bpelse_owner.VARIABLEATTACHMENT
  OWNER TO esb_bpelse_owner;
CREATE INDEX  VARIABLEATTACHMENTFKEY on  esb_bpelse_owner.VARIABLEATTACHMENT (stateid);

CREATE TABLE  esb_bpelse_owner.FOREACH (
  foreachid decimal,
  stateid varchar(128),
  counter integer,
  successes integer,
  startcount integer,
  finalcount integer,
  completioncount integer,
  primary key (foreachid, stateid)
) ;
ALTER TABLE  esb_bpelse_owner.FOREACH
  OWNER TO esb_bpelse_owner;
CREATE INDEX  FOREACHFKEY on  esb_bpelse_owner.FOREACH (stateid);

CREATE TABLE  esb_bpelse_owner.INSTANCECORRELATION (
    stateid varchar(128),
    inscorrid decimal,
    value varchar(4000),
    primary key (stateid, inscorrid)
);
ALTER TABLE  esb_bpelse_owner.INSTANCECORRELATION
  OWNER TO esb_bpelse_owner;
CREATE INDEX  INSTCORRFKEY on  esb_bpelse_owner.INSTANCECORRELATION (stateid);

CREATE TABLE  esb_bpelse_owner.ENGINECORRELATION (
    enginecorrid decimal,
    bpelid varchar(1028),
    engineid varchar(128),
    value varchar(4000),
    foreign key (engineid) references  esb_bpelse_owner.ENGINE(engineid)      
);
ALTER TABLE  esb_bpelse_owner.ENGINECORRELATION
  OWNER TO esb_bpelse_owner;
CREATE INDEX  ENGINECORRIDINDEX ON  esb_bpelse_owner.ENGINECORRELATION (enginecorrid);

CREATE TABLE  esb_bpelse_owner.LASTCHECKPOINT (
    stateid varchar(128),
    activityid decimal,
    timeval TIMESTAMP,
    pickcompositeactid decimal,
    branchinvokecounter decimal DEFAULT 0,
    primary key (stateid, activityid)
) ;
ALTER TABLE  esb_bpelse_owner.LASTCHECKPOINT
  OWNER TO esb_bpelse_owner;
CREATE INDEX  LCPFKEY on  esb_bpelse_owner.LASTCHECKPOINT (stateid);

CREATE TABLE  esb_bpelse_owner.SCOPE (
    stateid varchar(128),
    scopeid decimal,
    scopeguid varchar(128),
    parentscopeguid varchar(128),
    scopestate varchar(3),
    compensateid decimal,
    completionorder decimal,
    faultname varchar(4000),
    faultdata text,
    faultactivityid decimal,
    primary key (stateid, scopeguid)
);
ALTER TABLE  esb_bpelse_owner.SCOPE
  OWNER TO esb_bpelse_owner;
CREATE INDEX  SCOPEFKEY on  esb_bpelse_owner.SCOPE (stateid);

CREATE TABLE  esb_bpelse_owner.CRMP (
  stateid varchar(128),
  crmpinvokeid varchar(128),
  partnerlink varchar(1028),
  operation varchar(1028),
  bpelmessageexchange varchar(4000),
  replyvariableid decimal DEFAULT -1,
  responseobj text,
  primary key (crmpinvokeid)
);
ALTER TABLE  esb_bpelse_owner.CRMP
  OWNER TO esb_bpelse_owner;
CREATE INDEX  CRMPINDEX on  esb_bpelse_owner.CRMP (stateid, partnerlink, operation);
CREATE INDEX  CRMPREPLYVARINDEX on  esb_bpelse_owner.CRMP (replyvariableid, stateid);
CREATE INDEX  CRMPFKEY on  esb_bpelse_owner.CRMP (stateid);

CREATE TABLE  esb_bpelse_owner.SIMPLEVARIABLE (
    stateid varchar(128),
    varid decimal,
    stringvalue varchar(4000),
    scopeguid varchar(128),
    primary key (stateid, varid, scopeguid)
);
ALTER TABLE  esb_bpelse_owner.SIMPLEVARIABLE
  OWNER TO esb_bpelse_owner;
CREATE INDEX  SIMPLEVARFKEY on  esb_bpelse_owner.SIMPLEVARIABLE (stateid);

CREATE TABLE   esb_bpelse_owner.EVENTHANDLER (
  stateid varchar(128), 
  ehid varchar(128), 
  scopeguid varchar(128),
  eventmodelid decimal,
  status char(1) DEFAULT 'I',
    timeval TIMESTAMP,
    repeatevery decimal,
    primary key (stateid, ehid), 
  foreign key (stateid) references esb_bpelse_owner.STATE(stateid)
);
ALTER TABLE  esb_bpelse_owner.EVENTHANDLER
  OWNER TO esb_bpelse_owner;

CREATE TABLE  esb_bpelse_owner.PARTNERLINK (
    stateid varchar(128),
    plinkid decimal,
    value text,
  scopeguid varchar(128),     
    primary key (stateid, plinkid, scopeguid)
); 
ALTER TABLE  esb_bpelse_owner.PARTNERLINK
  OWNER TO esb_bpelse_owner;
CREATE INDEX  PARTNERLINKFKEY on  esb_bpelse_owner.PARTNERLINK (stateid);

CREATE TABLE  esb_bpelse_owner.OUTSTANDINGMSGEX (
    msgexid varchar(128),
    stateid varchar(128),
    bpelid varchar(1028),
  primary key (msgexid, bpelid)
);
ALTER TABLE esb_bpelse_owner.OUTSTANDINGMSGEX
  OWNER TO esb_bpelse_owner;