class postgresql::install ($tablespacedir) {
  if ($environment == 'cluster') {
    $tmp = regsubst($::ipaddr, '(.*)\..*', '\1.0') # regexp to get a network ip from an ip address
    $ipnetwork = "${tmp}/24"
  } else {
    $ipnetwork = '127.0.0.1/32'
  }

  package { 'postgresql-9.3': ensure => installed, } # install the package postgresql-9.3s

  service { 'postgresql':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    require    => [Package['postgresql-9.3']],
  }

  file { $tablespacedir:
    ensure  => directory, # create the directory /var/lib/postgresql/9.1/main/esb
    owner   => 'postgres', # the owner of the directory will be postgres
    recurse => true, # create all the missing directory in the path
    require => [Package['postgresql-9.3']],
  }

  file { '/tmp/scriptsql':
    ensure  => directory, # create the directory /tmp/scriptsql
    owner   => 'postgres', # the owner of the directory will be postgres
    mode    => '0744', # the rights are rwxr--r--
    require => Package['postgresql-9.3'],
  }

  file { '/tmp/scriptsql/create_bpelse_tables.sql': # set the sql scritpts which are in puppet:///modules/postgresql/ in
                                                    # /tmp/scriptsql
    source  => 'puppet:///modules/postgresql/create_bpelse_tables.sql',
    owner   => 'postgres', # owner the these files is postgres
    mode    => '0744', # rights are rwxr--r--
    require => File['/tmp/scriptsql'],
    before  => Exec['script-sql'],
  }

  file { '/tmp/scriptsql/create_bpelmonitor_tables.sql': # set the sql scritpts which are in puppet:///modules/postgresql/ in
                                                         # /tmp/scriptsql
    source  => 'puppet:///modules/postgresql/create_bpelmonitor_tables.sql',
    owner   => 'postgres', # owner the these files is postgres
    mode    => '0744', # rights are rwxr--r--
    require => File['/tmp/scriptsql'],
    before  => Exec['script-sql'],
  }

  file { '/tmp/scriptsql/create_bpelse_user.sql':
    content => template('postgresql/create_bpelse_user.sql.erb'),
    owner   => 'postgres', # owner the these files is postgres
    mode    => '0744', # rights are rwxr--r--
    require => File['/tmp/scriptsql'],
    before  => Exec['script-sql'],
  }

  file { '/tmp/scriptsql/create_openmq_user.sql':
    source  => 'puppet:///modules/postgresql/create_openmq_user.sql',
    owner   => 'postgres', # owner the these files is postgres
    mode    => '0744', # rights are rwxr--r--
    require => File['/tmp/scriptsql'],
    before  => Exec['script-sql'],
  }

  file { '/tmp/scriptsql/create_token_table.sql':
    ensure  => 'present',
    source  => 'puppet:///modules/openesb/create_token_table.sql',
    owner   => 'postgres',
    mode    => '0744',
    require => File['/tmp/scriptsql'],
    before  => Exec['script-sql'],
  }

  file { ["${tablespacedir}bpelse", "${tablespacedir}openmq"]:
    ensure  => directory,
    owner   => 'postgres',
    require => File["${tablespacedir}"],
    before  => Service['postgresql']
  }

  file { '/etc/postgresql/9.3/main/pg_hba.conf':
    content => template('postgresql/pg_hba.erb'),
    replace => true,
    require => [Package['postgresql-9.3']],
    notify  => Service['postgresql']
  }

  file { '/etc/postgresql/9.3/main/postgresql.conf':
    content => template('postgresql/postgresql.erb'),
    replace => true,
    require => [Package['postgresql-9.3']],
    notify  => Service['postgresql']
  }

  exec { 'script-sql':
    user    => 'postgres', # execute the scripts sql as postgres
    path    => '/usr/bin',
    command => 'psql -f /tmp/scriptsql/create_bpelse_user.sql;
psql -d esb_bpelse_db -f /tmp/scriptsql/create_bpelmonitor_tables.sql;
psql -d esb_bpelse_db -f /tmp/scriptsql/create_bpelse_tables.sql;
psql -f /tmp/scriptsql/create_openmq_user.sql;
psql -f /tmp/scriptsql/create_token_table.sql;
',
    require => Service['postgresql'],
  }

}

