class haproxy::install (
  $domain   = $::ipaddr,
  $openesb1 = hiera('ipopenesb1'),
  $openesb2 = hiera('ipopenesb2'),
  $httpdefaultport = hiera('openesb::httpbc::httpdefaultport'),
  $http_port=hiera('nginx::install::http_port')) {
  package { 'haproxy': ensure => installed }

  file { '/etc/default/haproxy':
    content => "ENABLED=1\n",
    require => Package['haproxy']
  }

  service { 'haproxy':
    ensure  => running,
    enable  => true,
    require => [Package['haproxy'], File['/etc/default/haproxy']],
  }

  file { '/etc/haproxy/haproxy.cfg':
    content => template('haproxy/haproxy.cfg.erb'),
    require => Package['haproxy'],
    notify  => Service['haproxy']
  }
}