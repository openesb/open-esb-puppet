class nginx::install ($http_port, $https_port) {
  $domain = $::ipaddress
  package { ["nginx", "openssl"]: ensure => installed, }

  file { "/tmp/sslKey.sh":
    source  => "puppet:///modules/nginx/sslKey.sh",
    mode    => '700',
    require => Package["nginx", "openssl"]
  }

  exec { "generate ssl certificate":
    command => "/bin/bash /tmp/sslKey.sh $domain",
    cwd     => "/etc/ssl/",
    require => File["/tmp/sslKey.sh"],
  }

  file { "/etc/nginx/nginx.conf":
    content => template('nginx/nginx.conf.erb'),
    replace => true,
    require => Exec["generate ssl certificate"]
  }

  service { 'nginx':
    enable     => true,
    ensure     => running,
    hasrestart => true,
    hasstatus  => true,
    require    => File["/etc/nginx/nginx.conf"]
  }
}