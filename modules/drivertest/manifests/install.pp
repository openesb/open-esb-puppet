class drivertest::install ($driverrepo, $openesbstudio) {
  package { ['git', 'ant', 'rsync']: ensure => present, }

  exec { 'get_drivertest':
    cwd     => '/vagrant/download',
    command => "/usr/bin/git clone $driverrepo",
    onlyif  => '/usr/bin/test ! -d /vagrant/download/openesb-driver-test',
    require => Package['git', 'ant', 'rsync'],
    before  => Exec['move_openesb-components']
  }

  exec { 'move_openesb-components':
    cwd     => '/vagrant/download',
    command => '/usr/bin/rsync -r openesb-driver-test /home/openesb/',
  }

  lib::wget { 'openesb-ide.zip':
    src         => "$openesbstudio",
    destination => '/vagrant/download/',
    onlyif      => "openesb-ide.zip",
    require     => Exec['move_openesb-components'],
  }

  lib::unzip { 'openesb-ide.zip':
    location    => '/vagrant/download/',
    destination => '/home/openesb',
    require     => Exec['openesb-ide.zipwget'],
  }

  file { '/home/openesb/openesb-driver-test/nbbuild/caps.build.properties':
    ensure  => present,
    owner   => openesb,
    source  => "puppet:///modules/drivertest/caps.build.properties",
    require => Exec['openesb-ide.zipunzip']
  }

  exec { 'create userdir':
    cwd     => '/home/openesb',
    user    => openesb,
    command => "/bin/mkdir -p userdir/config/J2EE/InstalledServers",
    require => File['/home/openesb/openesb-driver-test/nbbuild/caps.build.properties']
  }

  file { "/home/openesb/userdir/config/J2EE/InstalledServers/.nbattrs":
    ensure  => present,
    source  => "puppet:///modules/drivertest/nbattrs",
    require => Exec["create userdir"]
  }

  file { "/etc/environment":
    content => inline_template("<% if @architecture == \"i386\"-%>export ANT_OPTS=\"-Xmx1024m -Xms1024m -XX:MaxPermSize=512m -XX:PermSize=512m -XX:+CMSClassUnloadingEnabled\"<% else -%>export ANT_OPTS=\"-Xmx20418m -Xms2048m -XX:MaxPermSize=1024m -XX:PermSize=1024m -XX:+CMSClassUnloadingEnabled\"<% end -%>"
    ),
    replace => true
  }

  exec { 'launch_drivertest':
    cwd     => '/home/openesb/openesb-driver-test/driver-tests/',
    command => "/usr/bin/sudo /bin/su root -c \"/usr/bin/ant -v -f /home/openesb/openesb-driver-test/nbbuild/build.xml -Desb.netbeans.home=/home/openesb/netbeans/ -Dserver.password=admin -Desb.netbeans.user=/home/openesb/userdir -Djava.awt.headless=true test > /home/openesb/log.txt\"",
    timeout => 0,
    require => [
      Exec['openesb-ide.zipunzip', 'openesb-restart'],
      File['/home/openesb/userdir/config/J2EE/InstalledServers/.nbattrs', '/home/openesb/userdir/config/J2EE/InstalledServers/.nbattrs', 
        "/etc/environment"],
      Service['openmq']]
  }

}