class drivertest::log {
  exec { "openesb-log-file":
    command => "/bin/mkdir -p /vagrant/Drivertests/$operatingsystem/$operatingsystemrelease/$architecture/$environment/openesb/ ; /bin/cp /var/log/openesb/* /vagrant/Drivertests/$operatingsystem/$operatingsystemrelease/$architecture/$environment/openesb/",
    require => Exec['launch_drivertest']
  }

  exec { "openmq-log-file":
    command => "/bin/mkdir -p /vagrant/Drivertests/$operatingsystem/$operatingsystemrelease/$architecture/$environment/openmq/ ; /bin/cp /var/log/openmq/* /vagrant/Drivertests/$operatingsystem/$operatingsystemrelease/$architecture/$environment/openmq/",
    require => Exec['launch_drivertest']
  }

  exec { 'driver-test-log-file':
    command => "/bin/mkdir -p /vagrant/Drivertests/$operatingsystem/$operatingsystemrelease/$architecture/$environment/driver-test/",
    before  => Exec['launch_drivertest']
  }

  exec { 'driver-test-log':
    command => "/bin/cp /home/openesb/log.txt /vagrant/Drivertests/$operatingsystem/$operatingsystemrelease/$architecture/$environment/driver-test/",
    require => Exec['launch_drivertest']
  }
}
