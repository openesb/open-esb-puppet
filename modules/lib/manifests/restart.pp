# restart the component $name
define lib::restart {
  exec { "${name}restart":
    command => "/var/lib/openesb/bin/oeadmin.sh stop-jbi-component --user admin --passwordfile /vagrant/password.txt ${name} && /var/lib/openesb/bin/oeadmin.sh start-jbi-component --user admin --passwordfile /vagrant/password.txt ${name}",
    notify  => Exec['openesb-restart']
  }
}
