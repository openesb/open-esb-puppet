# Wget method
# default the user who exec the wget is root with a timeout of 10sec
define lib::wget ($src = '', $destination = '/tmp', $timeout = 0, $user = 'root', $onlyif) {
  if !defined(Package['wget']) {
    package { 'wget': ensure => installed } # ensure that wget is present on the machine
  }

  # Execute the wget
  exec { "${name}wget":
    cwd     => "$destination", # $destination is the absolute path of the direcotry where the command will be executed
    user    => $user, # $user is the name of the user which exec this command
    command => "/usr/bin/wget --timeout=$timeout $src$name", # $src$name is the full link of the file to download
    onlyif  => "/usr/bin/test ! -f $destination/$onlyif",
  }
}
