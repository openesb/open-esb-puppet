#deploy the component $name in the directory $cwd
define lib::deploy ($cwd) {
  exec { "${name}deploy":
    cwd     => "${cwd}",                                                                              #$cwd is the absolute path of the direcotry where the command will be executed
    command => "/bin/sh /var/lib/openesb/bin/oeadmin.sh install-jbi-component --user admin --passwordfile /vagrant/password.txt /vagrant/download/components/${name}", #install the jbi component named $name
    
  }
}
