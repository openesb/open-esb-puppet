# start the component $name
define lib::start {
  exec { "${name}start": command => "/var/lib/openesb/bin/oeadmin.sh start-jbi-component --user admin --passwordfile /vagrant/password.txt ${name}" }
}
