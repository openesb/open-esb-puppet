#Unzip a zip archive
define lib::unzip ($user = 'root', $location, $destination) {
  if !defined(Package['unzip']) {
    package { 'unzip': ensure => installed }                         #ensure that the unzip package is present on the machine
  }
#execute unzip cmd
  exec { "${name}unzip":      
    cwd     => "$location",                                          # $location is the absolute path of the direcotry where the command will be executed
    user   => $user,                                                 # $user is the name of the user which exec this command
    command => "/usr/bin/unzip -o $location/$name -d $destination",  # unzip the file named $name in the directory $destination
    require => Package['unzip'],
  }
}