# Download, deploy and start the databasebc component ; comments are in lib::*
# get the name of the databasebc component from the global.yaml file
class openesb::databasebc ($databasebc, $databasebcname, $databasebclink, $threads, $clusterjndidatabasename) {
  lib::wget { $databasebc:
    destination => '/vagrant/download/components',
    onlyif      => $databasebc,
    src         => "${::jenkins}$databasebclink",
    require     =>  File['/vagrant/download/components'],
  }

  lib::deploy { $databasebc:
    cwd     => '/var/lib/openesb/bin',
    require => [
      Exec["${databasebc}wget"],
      Service['openesb'],
      Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/databasebc':
    ensure  => present,
    replace => true,
    content => template('openesb/databasebc.erb'),
    require => Exec["${databasebcname}start"],
  }

  exec { 'configuration_databasebc':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $databasebcname /tmp/databasebc",
    require => File['/tmp/databasebc'],
  }

  lib::start { $databasebcname: require => Exec["${databasebc}deploy"] }

  lib::restart { $databasebcname: require => Exec['configuration_databasebc'] }
}
