# Install openesb on the system
# <-- install.pp -->

class openesb::install ($standalonelink, $standalonezip, $openesblaunchscript) {
  if ($environment == 'db') {
    file { "/var/lib/openesb/lib/ext/$::jdbcpsqlname":
      source  => "/vagrant/download/$::jdbcpsqlname",
      owner   => openesb,
      require => Exec["${standalonezip}unzip", "${::jdbcpgsqlink}wget"],
      before  => Service['openesb']
    }

    file { '/var/lib/openesb/config/context.xml':
      replace => true,
      content => template('openesb/context.xml.erb'),
      before  => Service['openesb'],
      require => Exec["${standalonezip}unzip"],
    }
  } elsif ($environment == 'cluster') {
    lib::wget { "$::jdbcpgsqlink":
      destination => '/vagrant/download/',
      onlyif      => $::jdbcpsqlname,
      src         => "",
    }

    file { "/var/lib/openesb/lib/ext/${::jdbcpsqlname}":
      source  => "/vagrant/download/${::jdbcpsqlname}",
      owner   => openesb,
      require => [Exec["${standalonezip}unzip", "${::jdbcpgsqlink}wget"], Exec['/tmp/download']],
    }

    file { '/var/lib/openesb/config/context.xml':
      replace => true,
      content => template('openesb/context.xml.erb'),
      before  => Service['openesb'],
      require => Exec["${standalonezip}unzip"],
    }
  }

  user { 'openesb':
    name       => 'openesb', # create a user named openesb
    uid        => 1200, # set the user's iud to 1200
    ensure     => present, # ensure user is created
    managehome => true, # create the home directory of the user
    system     => true, # informs puppet that it's a system user
  }

  file { '/var/lib/openesb':
    ensure  => directory, # create the directory /var/lib/openesb
    owner   => 'openesb', # set openesb owner of this directory
    mode    => '0644', # rights rw-r--r--
    require => User['openesb'],
  }

  lib::wget { "$standalonezip": # download the standalone archive refers to lib::wget for more details
    destination => '/vagrant/download/',
    onlyif      => $standalonezip,
    src         => "https://oss.sonatype.org/service/local/repositories/releases/content/net/open-esb/runtime/standalone/openesb-standalone-packaging/3.0.0/  ",
    require     => [File['/var/lib/openesb'],Exec['/tmp/download'], User['openesb']]
  }

  lib::unzip { "$standalonezip": # unzip the standalone archive refers to lib::unzip for more details
    location    => '/vagrant/download/',
    destination => '/var/lib/openesb/',
    require     => [Exec["${standalonezip}wget"], User['openesb'],Class['java::install']],
  }

  exec { 'droit_bashrc':
    cwd     => '/home/openesb',
    onlyif  => "/bin/grep -q \"export JAVA_HOME\" .bashrc; /usr/bin/test $? -eq 1",
    command => "/bin/echo \"export JAVA_HOME=$java::install::javahome\" >> .bashrc && /bin/echo \"export PATH=\$JAVA_HOME/bin:\$PATH\" >> .bashrc",
    require => User['openesb'],
  }

  file { '/etc/init.d/openesb': # create the file /etc/init.d/openesb
    content => template("openesb/$openesblaunchscript.erb"), # the file source will be openesb-puppet/modules/openesb/openesb
    mode    => '0744', # rights rwxr--r--
    owner   => 'openesb', # set openesb owner of this file
    require => [User['openesb'], Exec["${standalonezip}unzip"]]
  }

  exec { ['/tmp/download']:
    command => '/bin/mkdir /vagrant/download',
    onlyif  => '/usr/bin/test ! -d /vagrant/download'
  }

  file { '/vagrant/download/components':
    ensure  => directory,
    require => Exec['/tmp/download']
  }

  file { '/var/log/openesb':
    ensure  => link, # create a link from /var/log/openesb to /var/lib/openesb/logs
    target  => '/var/lib/openesb/logs',
    require => Exec["${standalonezip}unzip"]
  }

  service { 'openesb': # Start openesb service
    enable     => true, # Allow openesb to start on boot
    ensure     => running, # Force openesb to start now
    start      => '/etc/init.d/openesb start ; sleep 5',
    hasstatus  => true, # informs puppet that openesb has a status command
    status     => '/etc/init.d/openesb status |grep "is running"', # informs puppet that it should use this command to know if
    hasrestart => false, # openesb is running
    require    => [File['/etc/init.d/openesb'], Exec['droit_bashrc']]
  }

  exec { 'openesb-restart': command => '/etc/init.d/openesb stop ; sleep 3 ; /etc/init.d/openesb start; sleep 3 ' }
}
