class openesb::xsltse ($xsltse, $xsltsename, $xsltselink, $pollercount, $transformengine) {
  lib::wget { $xsltse:
    destination => '/vagrant/download/components',
    src         => "${::jenkins}$xsltselink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $xsltse,
  }

  lib::deploy { $xsltse:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${xsltse}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/xsltse':
    ensure  => present,
    replace => true,
    content => template('openesb/xsltse.erb'),
    require => Exec["${xsltsename}start"],
  }

  exec { 'configuration_xsltse':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --component $xsltsename /tmp/xsltse",
    require => File['/tmp/xsltse'],
  }

  lib::start { $xsltsename: require => Exec["${xsltse}deploy"] }

  lib::restart { $xsltsename: require => Exec['configuration_xsltse'] }
}
