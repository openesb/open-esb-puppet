# Download, deploy and start the jmsbc component ; comments are in lib::*
# get the name of the jmsbc component from the global.yaml file
class openesb::jmsbc (
  $jmsbc,
  $jmsbcname,
  $jmsbclink,
  $threads,
  $defaultredeliveryhandling,
  $forceconcurrencymode,
  $forcemaxconcurrentconsumers) {
  lib::wget { $jmsbc:
    destination => '/vagrant/download/components',
    src         => "$::jenkins$jmsbclink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $jmsbc,
  }

  lib::deploy { $jmsbc:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${jmsbc}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/jmsbc':
    ensure  => present, # create the config file /tmp/jmsbc
    replace => true, # rewrite the file if it is already there
    content => template('openesb/jmsbc.erb'), # create the file from this template
    require => Exec["${jmsbcname}start"],
  }

  exec { 'configuration_jmsbc':
    cwd     => '/var/lib/openesb/bin', # configure jmsbc component with the file /tmp/jmsbc
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $jmsbcname /tmp/jmsbc",
    require => File['/tmp/jmsbc'],
  }

  lib::start { $jmsbcname: require => Exec["${jmsbc}deploy"] }

  lib::restart { $jmsbcname: require => Exec['configuration_jmsbc'] }
}
