class openesb::wsdlsl ($wsdlsl, $wsdsllink) {
  lib::wget { $wsdlsl:
    destination => '/vagrant/download/components',
    src         => "${::jenkins}$wsdsllink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $wsdlsl,
  }

  exec { "deployinglibrary$wsdlsl":
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh /var/lib/openesb/bin/oeadmin.sh  install-jbi-shared-library --user admin --passwordfile /vagrant/password.txt /vagrant/download/components/${wsdlsl}",
    require => [Service['openesb'], Exec["${wsdlsl}wget"]],
  }
}