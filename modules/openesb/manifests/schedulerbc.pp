class openesb::schedulerbc (
  $schedulerbc,
  $schedulerbcname,
  $schedulerbclink,
  $pollercount,
  $quartzthreadcount,
  $usequartzramjobstore,
  $quartzpersistentjobstore) {
  lib::wget { $schedulerbc:
    destination => '/vagrant/download/components',
    src         => "${::jenkins}$schedulerbclink",
    require     => File['/vagrant/download/components'],
    onlyif      => $schedulerbc,
  }

  lib::deploy { $schedulerbc:
    cwd     => '/var/lib/openesb/bin',
    require => [
      Exec["${schedulerbc}wget"],
      Service['openesb'],
      Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/scheduler':
    ensure  => present,
    replace => true,
    content => template('openesb/scheduler.erb'),
    require => Exec["${schedulerbcname}start"],
  }

  exec { 'configuration_schedulerbc':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $schedulerbcname /tmp/scheduler",
    require => File['/tmp/scheduler'],
  }

  lib::start { $schedulerbcname: require => Exec["${schedulerbc}deploy"] }

  lib::restart { $schedulerbcname: require => Exec['configuration_schedulerbc'] }
}
