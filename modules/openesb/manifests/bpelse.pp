# Download, deploy and start the bpelse component ; comments are in lib::*
# get the name of the bpelse component from the global.yaml file
class openesb::bpelse (
  $bpelse,
  $bpelsename,
  $bpelselink,
  $debugenabled,
  $debugport,
  $persistenceenabled,
  $databasenonxajndiname,
  $databasexajndiname,
  $threadcount,
  $engineexpiryinterval,
  $waitingrequestlifespan,
  $monitoringenabled,
  $monitoringvariableenabled,
  $kpienabled,
  $transformengine,
  $validationenabled) {
  lib::wget { $bpelse:
    destination => '/vagrant/download/components',
    src         => "$::jenkins$bpelselink",
    onlyif      => $bpelse,
    require     => File['/vagrant/download/components'],
  }

  lib::deploy { $bpelse:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${bpelse}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/bpelse':
    ensure  => present,
    replace => true,
    content => template('openesb/bpelse.erb'),
    require => Exec["${bpelsename}start"],
  }

  exec { 'configuration_bpelse':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $bpelsename /tmp/bpelse",
    require => File['/tmp/bpelse'],
  }

  if ($environment == 'cluster') {
    $clusteroptsbpel = '-Dcom.sun.jbi.bpelse.isClustered=true -Dcom.sun.jbi.bpelse.isClustered.engineId=ID01'

    exec { 'javaopts_bpelse':
      command => "/bin/sed -i \'10i\\export JAVA_OPTS=\"\$JAVA_OPTS $::clusteroptsbpel\" #bpelse \' /etc/init.d/openesb",
      require => File['/etc/init.d/openesb']
    }
  }

  lib::start { $bpelsename: require => Exec["${bpelse}deploy"] }

  lib::restart { $bpelsename: require => Exec['configuration_bpelse'] }
}
