# Download, deploy and start the emailbc component ; comments are in lib::*
# get the name of the emailbc component from the global.yaml file
class openesb::emailbc ($emailbc, $emailbcname, $emailbclink, $pollercount) { # Number of threads listening on the NMR for message
                                                                              # exchanges
  lib::wget { $emailbc:
    destination => '/vagrant/download/components',
    onlyif      => $emailbc,
    src         => "$::jenkins$emailbclink",
    require     =>  File['/vagrant/download/components'],
  }

  lib::deploy { $emailbc:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${emailbc}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  exec { 'configuration_emailbc': # Configure emailbc pollercount with oeadmin command
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component sun-email-binding PollerCount=$pollercount",
    require => Exec["${emailbcname}start"],
  }

  lib::start { $emailbcname: require => Exec["${emailbc}deploy"] }

  lib::restart { $emailbcname: require => Exec['configuration_emailbc'] }
}
