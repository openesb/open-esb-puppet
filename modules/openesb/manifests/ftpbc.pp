# Download, deploy and start the ftpbc component ; comments are in lib::*
# get the name of the ftpbc component from the global.yaml file
class openesb::ftpbc (
  $ftpbc,
  $ftpbcname,
  $ftpbclink,
  $outboundthreads,
  $invoketimeout,
  $usepassiveftp,
  $useproxy,
  $proxyurl,
  $proxyuserid,
  $proxyuserpassword,
  $connectionpoolminsize,
  $connectionpoolmaxsize,
  $enablenmprops,
  $connectionmaxidletimeout) {
  lib::wget { $ftpbc:
    destination => '/vagrant/download/components',
    src         => "$::jenkins$ftpbclink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $ftpbc,
  }

  lib::deploy { $ftpbc:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${ftpbc}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/ftpbc':
    ensure  => present,
    replace => true,
    content => template('openesb/ftpbc.erb'),
    require => Exec["${ftpbcname}start"],
  }

  exec { 'configuration_ftpbc':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $ftpbcname /tmp/ftpbc",
    require => File['/tmp/ftpbc'],
  }

  if ($environment == 'cluster') {
    $ftpbc_cluster_opts = "-Dcom.sun.jbi.ftpbc.isClustered=true -Dcom.sun.jbi.ftpbc.token.persistence.url=jdbc:postgresql://$::ippostgresql:$::portpsql/ftp_db_persistence -Dcom.sun.jbi.ftpbc.token.db.jdbc.driver=org.postgresql.Driver"
    exec { 'javaopts_ftpbc':
      command => "/bin/sed -i \'10i\\export JAVA_OPTS=\"\$JAVA_OPTS $::ftpbc_cluster_opts\" #ftpbc\' /etc/init.d/openesb",
      require => File['/etc/init.d/openesb']
    }
  }

  lib::start { $ftpbcname: require => Exec["${ftpbc}deploy"] }

  lib::restart { $ftpbcname: require => Exec['configuration_ftpbc'] }
}
