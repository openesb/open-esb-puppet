# Download, deploy and start the httpbc component ; comments are in lib::*
# get the name of the httpbc component from the global.yaml file
class openesb::httpbc (
  $httpbc,
  $httpbcname,
  $httpbclink,
  $outboundthreads,
  $inboundthreads,
  $inboundreplythreads,
  $httpdefaultport,
  $httpsdefaultport,
  $clientauthenabled,
  $amconfigdirectory,
  $amclasspath,
  $proxytype,
  $proxyhost,
  $proxyport,
  $nonproxyhosts,
  $proxyusername,
  $proxypassword,
  $usejvmproxysettings,
  $validhostnames) {
  lib::wget { $httpbc:
    destination => '/vagrant/download/components',
    src         => "$::jenkins$httpbclink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $httpbc,
  }

  lib::deploy { $httpbc:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${httpbc}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/httpbc':
    ensure  => present, # create the config file of httpbc
    replace => true, # rewrite the file if it is already there
    content => template('openesb/httpbc.erb'), # create the file from this template
    require => Exec["${httpbcname}start"],
  }

  exec { 'configuration_httpbc':
    cwd     => '/var/lib/openesb/bin', # configure httpbc component with the file /tmp/httpbc
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $httpbcname /tmp/httpbc",
    require => File['/tmp/httpbc'],
  }

  lib::start { $httpbcname: require => Exec["${httpbc}deploy"] }

  lib::restart { $httpbcname: require => Exec['configuration_httpbc'] }
}
