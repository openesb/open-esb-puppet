class openesb::wsdlextlib ($wsdlextlib, $wsdlextliblink) {
  lib::wget { $wsdlextlib:
    destination => '/vagrant/download/components',
    src         => "${::jenkins}$wsdlextliblink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $wsdlextlib,
  }

  exec { "deployinglibrary$wsdlextlib":
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh /var/lib/openesb/bin/oeadmin.sh  install-jbi-shared-library --user admin --passwordfile /vagrant/password.txt /vagrant/download/components/${wsdlextlib}",
    require => [Service['openesb'], Exec["${wsdlextlib}wget"]],
  }
}