
class openesb::pojose ($pojose, $pojosename, $pojoselink, $corethreadpoolsize, $maxthreadpoolsize, $threadpoolblockingqueuesize) {
  lib::wget { $pojose:
    destination => '/vagrant/download/components',
    src         => "${::jenkins}$pojoselink",
    onlyif      => $pojose,
    require     =>  File['/vagrant/download/components'],
  }

  lib::deploy { $pojose:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${pojose}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/pojose':
    ensure  => present,
    replace => true,
    content => template('openesb/pojose.erb'),
    require => Exec["${pojosename}start"],
  }

  exec { 'configuration_pojose':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $pojosename /tmp/pojose",
    require => File['/tmp/pojose'],
  }

  lib::start { $pojosename: require => Exec["${pojose}deploy"] }

  lib::restart { $pojosename: require => Exec['configuration_pojose'] }
}

