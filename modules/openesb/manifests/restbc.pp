class openesb::restbc (
  $restbc,
  $restbcname,
  $restbclink,
  $nmrthreadpoolsize,
  $nmrmaxthreadpoolsize,
  $defaulthttplistenerport,
  $defaulthttplistenerthreads,
  $defaulthttpslistenerport,
  $defaulthttpslistenerthreads,
  $truststorepassword,
  $keystorepassword,
  $enablehostnameverifier,) {
  lib::wget { $restbc:
    destination => '/vagrant/download/components',
    src         => "${::jenkins}$restbclink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $restbc,
  }

  lib::deploy { $restbc:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${restbc}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/restbc':
    ensure  => present,
    replace => true,
    content => template('openesb/restbc.erb'),
    require => Exec["${restbcname}start"],
  }

  exec { 'configuration_restbc':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $restbcname /tmp/restbc",
    require => File['/tmp/restbc'],
  }

  lib::start { $restbcname: require => Exec["${restbc}deploy"] }

  lib::restart { $restbcname: require => Exec['configuration_restbc'] }
}
