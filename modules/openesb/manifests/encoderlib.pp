class openesb::encoderlib ($encoderlib, $encoderliblink) {
  lib::wget { $encoderlib:
    destination => '/vagrant/download/components',
    onlyif      => $encoderlib,
    src         => "${::jenkins}$encoderliblink",
    require     =>  File['/vagrant/download/components'],
  }

  exec { "deployinglibrary$encoderlib":
    command => "/bin/sh /var/lib/openesb/bin/oeadmin.sh  install-jbi-shared-library --user admin --passwordfile /vagrant/password.txt /vagrant/download/components/${encoderlib}",
    require => [Service['openesb'], Exec["${encoderlib}wget"]],
  }
}