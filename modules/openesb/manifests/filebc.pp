# Download, deploy and start the filebc component ; comments are in lib::*
# get the name of the filebc component from the global.yaml file
class openesb::filebc ($filebc, $filebcname, $filebclink, $threads, $ibworkerthreads) {
  lib::wget { $filebc:
    destination => '/vagrant/download/components',
    src         => "$::jenkins$filebclink",
    require     =>  File['/vagrant/download/components'],
    onlyif      => $filebc,
  }

  lib::deploy { $filebc:
    cwd     => '/var/lib/openesb/bin',
    require => [Exec["${filebc}wget"], Service['openesb'], Class['openesb::encoderlib', 'openesb::wsdlsl', 'openesb::wsdlextlib']],
  }

  file { '/tmp/filebc':
    ensure  => present,
    replace => true,
    content => template('openesb/filebc.erb'),
    require => Exec["${filebcname}start"],
  }

  exec { 'configuration_filebc':
    cwd     => '/var/lib/openesb/bin',
    command => "/bin/sh oeadmin.sh set-jbi-component-configuration --user admin --passwordfile /vagrant/password.txt --component $filebcname /tmp/filebc",
    require => File['/tmp/filebc'],
  }

  lib::start { $filebcname: require => Exec["${$filebc}deploy"] }

  lib::restart { $filebcname: require => Exec['configuration_filebc'] }
}
