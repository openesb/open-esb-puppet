define nfs::share ($path, $allowed, $options = '') {
  include nfs::server

  file { "/etc/exports.d/${name}":
    content => "$path $allowed($options)\n",
    notify  => Exec['update-etc-exports'],
  }
}