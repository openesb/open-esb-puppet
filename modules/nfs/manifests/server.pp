# Install the nfs server
class nfs::server ($sharedbyserv) {
  $ipopenesb1 = hiera('ipopenesb1')
  $ipopenesb2 = hiera('ipopenesb2')

  package { ['nfs-kernel-server']: ensure => installed } # ensure that portmap's, nfs-common's and nfs-kernel-server's packages are
                                                         # installed



  user { 'openesb':
    name       => 'openesb', # create a user named openesb
    uid        => 1200, # set the user's iud to 1200
    ensure     => present, # ensure user is created
    managehome => true, # create the home directory of the user
    system     => true, # informs puppet that it's a system user
    before     => Package['nfs-kernel-server'],
  }

  file { "$sharedbyserv":
    ensure  => directory,
    owner   => openesb,
    require => User['openesb'],
  }

  service { 'nfs-kernel-server':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    require    => Package['nfs-kernel-server']
  }

  file { '/etc/exports.d': ensure => directory, }

  exec { 'update-etc-exports':
    command     => '/bin/cat /etc/exports.d/* >/etc/exports',
    notify      => Service['nfs-kernel-server'],
    refreshonly => true,
  }

  nfs::share { 'openesb1':
    path    => "$sharedbyserv",
    allowed => "$ipopenesb1",
    options => 'rw,sync,no_root_squash',
    require => File['/etc/exports.d'],
  }

  nfs::share { 'openesb2':
    path    => "$sharedbyserv",
    allowed => "$ipopenesb2",
    options => 'rw,sync,no_root_squash',
    require => File['/etc/exports.d'],
  }

}