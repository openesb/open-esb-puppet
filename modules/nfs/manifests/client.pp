# Install the nfs client and mount the shared directory
class nfs::client (
  $ipnfs          = hiera('ipnfs'), # get the ip of the nfs server from the hiera file
  $sharedbyserv   = hiera('nfs::server::sharedbyserv'), # get the shared directory of the nfs server from the hiera file
  $mountpoint, # get the path where the shared directory must be mounted from the hiera file
  $client_options = 'auto',
  $portmapsname) {

  package { "$portmapsname" : ensure => installed } # ensure that portmap's and nfs-common's packages are installed



  service { 'nfs-common':
    ensure    => running, # start statd service
    enable    => true, # allow statd to start on boot
    name      => 'statd', # informs puppet that the name of the service is statd
    hasstatus => true, # informs puppet that statd has a status command
    require   => Package['portmap'],
  }

  service { 'portmap':
    ensure    => running, # start rpcbind service
    enable    => true, # allow rpcbind to start on boot
    hasstatus => false, # informs puppet that rpcbind has a status command
    name      => $portmapsname, # informs puppet that the name of the service is rpcbind
    require   => Package['portmap'],
  }

  exec { 'create mount point': # create the directories where the nfs will be mounted
    command => "/bin/mkdir -p $mountpoint", }

  mount { "shared by server": # mount the nfs directory
    device   => "${ipnfs}:${sharedbyserv}", # device to mount (ip of server + shared directory)
    fstype   => 'nfs', # file system type : nfs
    options  => $client_options, # mouting otpions
    name     => $mountpoint, # where the file system will be mounted
    remounts => false, # don't remount the file system
    atboot   => true, # allow puppet to mount the filesystem on boot
    ensure   => mounted, # mount the file system
    require  => [Service['nfs-common', 'portmap'], Exec['create mount point']]
  }

}