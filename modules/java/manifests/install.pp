# install the jdk 7 on the machine
class java::install ($jdk,$javahome){
  
  
  case $operatingsystem {
    ubuntu: {
          exec { 'java_ppa':
               command => "sudo /usr/bin/add-apt-repository ppa:webupd8team/java",
               path => "/usr/bin",
                  }
  
          exec { 'java_update':
               command => "sudo /usr/bin/apt-get update",
               path => "/usr/bin",
            require    => [Exec['java_ppa']],
            }
				  
				  exec { 'java_licence':
				       command => "echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections",
               path => "/usr/bin:/bin",
				      require    => [Exec['java_update']],
				  }
				  
				   package { 'oracle-java7-installer':
              ensure => installed,
              require    => [Exec['java_licence']],
          }
				  
  
  
         package { 'oracle-java7-set-default':
              ensure => installed,
              require    => [Package['oracle-java7-installer']],
          }
  
				
        }
         default: {
           
					  package { 'openjdk-7-jdk':                                            #install the java 7 jdk on the machine
					    ensure => installed,
					    name   => $jdk,
					  }

         }
  }
 
}
