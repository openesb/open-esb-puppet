# Install openmq on the system

class openmq::install ($openmq_name, $openmq_link, $openmqlaunchscript, $clusterHa = "", $clusterid = "") {
  user { 'openmq':
    name       => 'openmq', # create a user named openmq
    ensure     => present, # ensure user is created
    uid        => 1201,
    managehome => true, # create the home directory for the user
    system     => true, # informs puppet that it's a system user
  }

  if ($::environment == 'cluster') {
    exec { ['/tmp/download']:
      command => '/bin/mkdir /vagrant/download',
      onlyif  => '/usr/bin/test ! -d /vagrant/download'
    }
  }

  lib::wget { "$openmq_name": # download openmq's archive refers to lib::wget for more details
    destination => '/vagrant/download/',
    onlyif      => $openmq_name,
    src         => "$openmq_link",
    require     => [User['openmq'], Exec['/tmp/download']],
  }

  lib::unzip { "$openmq_name": # unzip openmq's archive refers to lib::unzip for more details
    location    => '/vagrant/download/',
    destination => '/var/lib/',
    require     => [Exec["${openmq_name}wget"], User['openmq'], File['/var/lib/MessageQueue4_5'],Class['java::install']],
  }

  file { '/var/lib/MessageQueue4_5':
    ensure  => directory,
    owner   => openmq,
    require => User['openmq']
  }

  file { '/var/lib/openmq':
    ensure  => link, # create a link from /var/lib/openmq to /var/lib/MessageQueue4_5
    owner   => openmq,
    recurse => true,
    target  => '/var/lib/MessageQueue4_5',
    require => [Exec["${openmq_name}unzip"], User['openmq']],
  }

  file { '/var/log/openmq':
    ensure  => directory,
    owner   => openmq,
    require => File['/var/lib/openmq'],
  }

  exec { 'droit_bashrc_openmq': # export $JAVA_HOME path for openmq user
    cwd     => '/home/openmq',
    onlyif  => "/bin/grep -q \"export JAVA_HOME\" .bashrc; /usr/bin/test $? -eq 1",
    command => "/bin/echo \"export JAVA_HOME=$java::install::javahome\" >> .bashrc && /bin/echo \"export PATH=\$JAVA_HOME/bin:\$PATH\" >> .bashrc",
    require => User['openmq'],
  }

  file { '/etc/init.d/openmq': # create the file etc/init.d/openmq
    ensure  => present,
    content => template("openmq/$openmqlaunchscript.erb"), # the file source will be openesb-puppet/modules/openmq/openmq
    mode    => '0744', # rights rwxr--r--
    owner   => 'openmq', # set openmq owner of this file
    require => File['/var/lib/openmq'],
  }

  exec { "-Xms256k":
    command => "/bin/sed -i -e \"s/_def_jvm_args=\\\"-Xms192m -Xmx192m -Xss192k\\\"/_def_jvm_args=\\\"-Xms192m -Xmx192m -Xss256k\\\"/g\" /var/lib/openmq/mq/bin/imqbrokerd",
    before  => Service['openmq'],
    require => File['/var/lib/openmq'],
  }

  service { 'openmq':
    ensure     => running,
    enable     => true,
    hasrestart => false,
    status     => '/etc/init.d/openmq status |grep "is running"',
    require    => [Exec['-Xms256k'], File['/etc/init.d/openmq']],
  }

  if ($environment == 'cluster' or $environment == 'db') {
    exec { 'create imqbroker directory':
      command => '/bin/mkdir -p /var/lib/openmq/var/mq/instances/imqbroker/props',
      user    => openmq,
      require => File['/var/lib/openmq']
    }

    file { '/var/lib/openmq/var/mq/instances/imqbroker/props/config.properties':
      owner   => openmq,
      replace => true,
      content => template('openmq/config.properties.erb'),
      require => Exec['create imqbroker directory'],
      before  => Service['openmq']
    }

    exec { 'no loopback ip address':
      command => "/bin/sed -i -e \"s/127.0.1.1/${::ipaddr}/g\" /etc/hosts",
      before  => User['openmq']
    }

    lib::wget { "$::jdbcpgsqlink":
      destination => '/vagrant/download/',
      onlyif      => $::jdbcpgsqlink,
      src         => "",
      before      => File["/var/lib/openmq/mq/lib/ext/$::jdbcpsqlname"],
      require     => Exec['/tmp/download']
    }

    file { "/var/lib/openmq/mq/lib/ext/$::jdbcpsqlname":
      source  => "/vagrant/download/$::jdbcpsqlname",
      owner   => openmq,
      require => Exec["${openmq_name}unzip"],
    }

  }
}
