# -*- mode: ruby -*-
# vi: set ft=ruby :
# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|


  config.vm.define "cluster_nginx" do |nginx|
	nginx.vm.hostname = 'nginx'
	config.vm.network "private_network", ip: "192.168.50.8"	
	nginx.vm.box = 'ubuntu/precise64'
	config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = " --environment=cluster"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "cluster_nginx", "--memory", "2048"]
     end
 	
  end

  config.vm.define "cluster_postgresql" do |postgresql|
	postgresql.vm.hostname = 'postgresql'
	config.vm.network "private_network", ip: "192.168.50.6"	
	postgresql.vm.box = 'ubuntu/precise64'
	config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug --environment=cluster"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "cluster_postgresql", "--memory", "2048"]
     end

  end
  
  config.vm.define "cluster_nfs" do |nfs|
	nfs.vm.hostname = 'nfs'
  	config.vm.network "private_network", ip: "192.168.50.4"
  	nfs.vm.box = 'ubuntu/precise64'
  	config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = " --environment=cluster"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "cluster_nfs", "--memory", "2048"]
     end

  end
  
  config.vm.define "cluster_openesb1" do |openesb1|
	openesb1.vm.hostname = 'openesb1'
	config.vm.network "private_network", ip: "192.168.50.3"	
	openesb1.vm.box = 'ubuntu/precise64'
	config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--environment=cluster"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "cluster_openesb1", "--memory", "2048"]
     end
  end

  config.vm.define "cluster_openmq1" do |openmq1|
	openmq1.vm.hostname = 'openmq1'
	config.vm.network "private_network", ip: "192.168.50.5"	
	openmq1.vm.box = 'ubuntu/precise64'
	config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug --environment=cluster"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "cluster_openmq1", "--memory", "2048"]
     end

  end

  config.vm.define "cluster_openesb2" do |openesb2|
	openesb2.vm.hostname = 'openesb2'
	config.vm.network "private_network", ip: "192.168.50.2"	
	openesb2.vm.box = 'ubuntu/precise64'
	config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--environment=cluster"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "cluster_openesb2", "--memory", "2048"]
     end

  end

  config.vm.define "cluster_openmq2" do |openmq2|
	openmq2.vm.hostname = 'openmq2'
	config.vm.network "private_network", ip: "192.168.50.7"	
	openmq2.vm.box = 'ubuntu/precise64'
	config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug --environment=cluster"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "cluster_openmq2", "--memory", "2048"]
     end

  end
  
  # Ubuntu 12.04 LTS 64 bits
  config.vm.define "standalone_precise64" do |stdalnpre64|
    stdalnpre64.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.10"	
    stdalnpre64.vm.box = 'ubuntu/precise64'
    #config.vm.box_url = "http://192.168.1.44/base_precise64"
     config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = ""
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "standalone_precise64", "--memory", "2048"]
     end

  end

  # Ubuntu 12.04 LTS 32 bits
  config.vm.define "standalone_precise32" do |stdalnpre32|
    stdalnpre32.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.20"	    
    stdalnpre32.vm.box = 'ubuntu/precise32'
   # config.vm.box_url = "http://192.168.1.44/images/base_precise32"
     config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "standalone_precise32", "--memory", "2048"]
     end

  end

  # Ubuntu 14.04 LTS 64 bits  
  config.vm.define "standalone_trusty64" do |stdalntrst64|
    stdalntrst64.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.30"	
    stdalntrst64.vm.box = 'ubuntu/trusty64'
    config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--verbose --debug"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "standalone_trusty64", "--memory", "2048"]
     end

  end

  # Ubuntu 14.04 LTS 32 bits
  config.vm.define "standalone_trusty32" do |stdalntrst64|
    stdalntrst64.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.40"	
    stdalntrst64.vm.box = 'ubuntu/trusty32'
   # config.vm.box_url = "http://192.168.1.44/images/base_trusty32"
     config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
	 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "standalone_trusty32", "--memory", "2048"]
     end

  end

  # CentOS 6 64 bits
  config.vm.define "standalone_centos_6_64" do |stdalnctos64|
    stdalnctos64.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.50"	
    stdalnctos64.vm.box = 'puppetlabs/centos-6.5-64-puppet'
    
    
     config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
  end

  # CentOS 6 32 bits
  config.vm.define "standalone_centos_6_32" do |stdalnctos32|
    stdalnctos32.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.60"	
    stdalnctos32.vm.box = 'puppetlabs/centos-6.5.32-puppet'
     config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "standalone_centos_6_32", "--memory", "2048"]
     end

  end
  
  # Debian 7 64 bits
  config.vm.define "standalone_wheezy64" do |stdalnwhz64|
    stdalnwhz64.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.70"
    stdalnwhz64.vm.box = 'ekino/debian_wheezy_devop'
     config.vm.provision "puppet" do |puppet|
	  puppet.manifests_path = "manifests"
  	  puppet.manifest_file  = "site.pp"
 	  puppet.options = "--debug"
 	  puppet.hiera_config_path = "hiera.yaml"
  	  puppet.module_path = "modules" 
 	  puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "standalone_wheezy64", "--memory", "2048"]
     end

  end
  
  
  
    config.vm.define "test_24" do |test_24|
    test_24.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.24"	
    test_24.vm.box = 'ubuntu/trusty64'
    config.vm.provision "puppet" do |puppet|
  	  	puppet.manifests_path = "manifests"
 	 	puppet.manifest_file  = "site.pp"
		puppet.options = "--debug --environment=test_2.4"
	    puppet.hiera_config_path = "hiera.yaml"
		puppet.module_path = "modules" 
		puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_24", "--memory", "2048"]
     end

  end
  
   # Ubuntu 12.04 LTS 64 bits
  config.vm.define "db_precise64" do |dbprec64|
    dbprec64.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.71"	
    dbprec64.vm.box = 'base_precise64'
    config.vm.box_url = "http://192.168.1.44/images/base_precise64"
     config.vm.provision "puppet" do |puppet|
				puppet.manifests_path = "manifests"
			  	puppet.manifest_file  = "site.pp"
			 	puppet.options = "--environment=db"
			 	puppet.hiera_config_path = "hiera.yaml"
			  	puppet.module_path = "modules" 
			 	puppet.temp_dir             = "/tmp/openesb-puppet"
			   	puppet.working_directory    = "/tmp/openesb-puppet"
     end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "db_precise64", "--memory", "2048"]
     end
  end

  config.vm.define "db_precise32" do |dbprec32|
    dbprec32.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.72"	
    dbprec32.vm.box = 'base_precise32'
    config.vm.box_url = "http://192.168.1.44/images/base_precise32"
     config.vm.provision "puppet" do |puppet|
				puppet.manifests_path = "manifests"
			  	puppet.manifest_file  = "site.pp"
			 	puppet.options = "--environment=db"
			 	puppet.hiera_config_path = "hiera.yaml"
			  	puppet.module_path = "modules" 
			 	puppet.temp_dir             = "/tmp/openesb-puppet"
			   	puppet.working_directory    = "/tmp/openesb-puppet"
     end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "db_precise32", "--memory", "2048"]
     end
  end

  config.vm.define "db_trusty64" do |dbtru64|
    dbtru64.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.73"	
    dbtru64.vm.box = 'base_trusty64'
    config.vm.box_url = "http://192.168.1.44/images/base_trusty64"
     config.vm.provision "puppet" do |puppet|
				puppet.manifests_path = "manifests"
			  	puppet.manifest_file  = "site.pp"
			 	puppet.options = "--environment=db"
			 	puppet.hiera_config_path = "hiera.yaml"
			  	puppet.module_path = "modules" 
			 	puppet.temp_dir             = "/tmp/openesb-puppet"
			   	puppet.working_directory    = "/tmp/openesb-puppet"
     end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "db_trusty64", "--memory", "2048"]
     end
  end
  
  config.vm.define "db_trusty32" do |dbtru32|
    dbtru32.vm.hostname = 'standalone'
    config.vm.network "private_network", ip: "192.168.50.74"	
    dbtru32.vm.box = 'base_trusty32'
    config.vm.box_url = "http://192.168.1.44/images/base_trusty32"
     config.vm.provision "puppet" do |puppet|
				puppet.manifests_path = "manifests"
			  	puppet.manifest_file  = "site.pp"
			 	puppet.options = "--environment=db"
			 	puppet.hiera_config_path = "hiera.yaml"
			  	puppet.module_path = "modules" 
			 	puppet.temp_dir             = "/tmp/openesb-puppet"
			   	puppet.working_directory    = "/tmp/openesb-puppet"
     end
 config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "db_trusty32", "--memory", "2048"]
     end
  end

config.vm.define "test_23_precise64" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.41"	
    test.vm.box = 'base_precise64'
    config.vm.box_url = "http://192.168.1.44/images/base_precise64"
    config.vm.provision "puppet" do |puppet|
  	  	puppet.manifests_path = "manifests"
 	 	puppet.manifest_file  = "site.pp"
		puppet.options = "--debug --environment=test"
	    	puppet.hiera_config_path = "hiera.yaml"
		puppet.module_path = "modules" 
		puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end

     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_23_precise64", "--memory", "8196"]
     end

  end

  config.vm.define "test_23_precise32" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.42"
    test.vm.box = 'base_trusty64'
    config.vm.box_url = "http://192.168.1.44/images/base_trusty64"
    config.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=test"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
        puppet.working_directory    = "/tmp/openesb-puppet"
        end

     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_23_precise32", "--memory", "8196"]
     end

  end

  config.vm.define "test_23_trusty64" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.43"
    test.vm.box = 'base_trusty64'
    config.vm.box_url = "http://192.168.1.44/images/base_trusty64"
    config.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=test"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
    		    puppet.working_directory    = "/tmp/openesb-puppet"
        end

     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_23_trusty64", "--memory", "8196"]
     end

  end
  config.vm.define "test_23_trusty32" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.44"
    test.vm.box = 'base_trusty32'
    config.vm.box_url = "http://192.168.1.44/images/base_trusty32"
    config.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=test"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
    		    puppet.working_directory    = "/tmp/openesb-puppet"
        end

     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_23_trusty_32", "--memory", "8196"]
     end

  end

config.vm.define "test_24_precise64" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.61"	
    test.vm.box = 'ubuntu/precise64'
    config.vm.box_url = "http://192.168.1.44/images/base_precise64"
    config.vm.provision "puppet" do |puppet|
  	  	puppet.manifests_path = "manifests"
 	 	puppet.manifest_file  = "site.pp"
		puppet.options = "--debug --environment=test_24"
	    	puppet.hiera_config_path = "hiera.yaml"
		puppet.module_path = "modules" 
		puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end

     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_24_precise64", "--memory", "8196"]
     end

  end

  config.vm.define "test_24_precise32" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.62"
    test.vm.box = 'base_precise32'
    config.vm.box_url = "http://192.168.1.44/images/base_precise32"
    config.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=test_24"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
        puppet.working_directory    = "/tmp/openesb-puppet"
        end

     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_24_precise32", "--memory", "8196"]
     end

  end

  config.vm.define "test_24_trusty64" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.63"
    test.vm.box = 'base_trusty64'
    config.vm.box_url = "http://192.168.1.44/images/base_trusty64"
    config.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=test_24"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
    		    puppet.working_directory    = "/tmp/openesb-puppet"
        end
     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_24_trusty64", "--memory", "8196"]
     end
  end
  
  config.vm.define "test_24_trusty32" do |test|
    test.vm.hostname = 'test'
    config.vm.network "private_network", ip: "192.168.50.64"
    test.vm.box = 'base_trusty32'
    config.vm.box_url = "http://192.168.1.44/images/base_trusty32"
    config.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=test_24"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
    		    puppet.working_directory    = "/tmp/openesb-puppet"
        end
     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "test_24_trusty_32", "--memory", "8196"]
     end
  end
  
config.vm.define "base_precise64" do |test|
    test.vm.hostname = 'standalone'
    test.vm.box = 'ubuntu/precise64'
    config.vm.provision "puppet" do |puppet|
  	  	puppet.manifests_path = "manifests"
 	 	puppet.manifest_file  = "site.pp"
		puppet.options = "--debug --environment=base"
	    	puppet.hiera_config_path = "hiera.yaml"
		puppet.module_path = "modules" 
		puppet.temp_dir             = "/tmp/openesb-puppet"
    	puppet.working_directory    = "/tmp/openesb-puppet"
  	end
     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "base_precise64", "--memory", "2048"]
     end
  end

  config.vm.define "base_precise32" do |test|
    test.vm.hostname = 'standalone'
    test.vm.box = 'ubuntu/precise32'
    config.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = " --debug --environment=base"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
        puppet.working_directory    = "/tmp/openesb-puppet"
        end
     config.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "base_precise32", "--memory", "2048"]
     end
  end

  config.vm.define "base_trusty64" do |bt64|
    bt64.vm.hostname = 'standalone'
    bt64.vm.box = 'ubuntu/trusty64'
    bt64.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=base"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
    		    puppet.working_directory    = "/tmp/openesb-puppet"
        end
     bt64.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "base_trusty64", "--memory", "2048"]
     end
  end
  
  config.vm.define "base_trusty32" do |bt32|
    bt32.vm.hostname = 'standalone'
    bt32.vm.box = 'ubuntu/trusty32'
    bt32.vm.provision "puppet" do |puppet|
                puppet.manifests_path = "manifests"
                puppet.manifest_file  = "site.pp"
                puppet.options = "--debug --environment=base"
                puppet.hiera_config_path = "hiera.yaml"
                puppet.module_path = "modules"
                puppet.temp_dir             = "/tmp/openesb-puppet"
    		    puppet.working_directory    = "/tmp/openesb-puppet"
        end
     bt32.vm.provider "virtualbox" do |vb|
          vb.customize ["modifyvm", :id,"--name", "base_trusty32", "--memory", "2048"]
     end
  end
  
  #default confs
  
  config.vm.provision "shell", path: "puppet_install.sh"
    
  config.vm.box = "hashicorp/precise64"

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "2048"]
  end
 
end

