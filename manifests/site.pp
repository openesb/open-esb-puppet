case $operatingsystem { # Install only on Ubuntu 12.04 / 14.04 & centos OS
  'ubuntu' : {
    if (!($operatingsystemrelease in ['12.04', '14.04'])) {
      fail('Unsuported operating system')
    }
  }
  'centos' : {
  }
  default  : {
    fail('Unsuported operating system')
  }
}

$ippostgresql = hiera('ippostgresql', 'localhost')
$jdbcpgsqlink = hiera('jdbcpgsqlink')
$jdbcpsqlname = hiera('jdbcpsqlname', '')
$portpsql = hiera('portpsql', '')
$iface = hiera('defaultinterface', 'eth1')
$jenkins = hiera('jenkins')
$ipaddr = inline_template("<%= @ipaddress_${iface}%>")
hiera_include('os_specific_classes')

node "openesb1" {
  hiera_include('components_classes')
  hiera_include('openesb_cluster_classes')

}

node "openesb2" {
  hiera_include('components_classes')
  hiera_include('openesb_cluster_classes')
}

node "openmq1" {
  $brokerid = 'IDO1'
  hiera_include('openmq_cluster_classes')
}

node "openmq2" {
  $brokerid = 'IDO2'
  hiera_include('openmq_cluster_classes')
}

node "nginx" {
  hiera_include('proxy_cluster_classes')
}

node "nfs" {
  hiera_include('cluster_nfs_classes')
}

node "postgresql" {
  hiera_include('cluster_postgresql_classes')
}

node 'standalone' {
  if ($environment == 'db') {
    hiera_include('database_classe')
  }
  $brokerid = 'ID00'
  hiera_include('components_classes')
  hiera_include('standalone_classes')
}

node 'test' {
  $brokerid = 'ID00'
  hiera_include('components_classes')
  hiera_include('standalone_classes')
  hiera_include('test_classe')
}
