#!/bin/bash
sed -i 's/^mesg n$/tty -s \&\& mesg n/g' /root/.profile
if [ -f /usr/bin/lsb_release ]
then
    os=`lsb_release -d | cut -f 2 | cut -d' ' -f 1`
elif [ -f /etc/redhat-release ]
then
    os=`cat /etc/redhat-release | cut -d" " -f 1`
else
    os=`cat /etc/os-release | grep NAME | cut -d= -f2 | cut -d\" -f2 | cut -d" " -f1 | head -n 1`
fi

if [[ ( $os = "Ubuntu" ) || ( $os = "Debian" ) ]]
then
    is_puppet_installed="dpkg -l | grep -c puppet"
    version=`lsb_release -c | cut -f 2`
    install_puppet="wget apt.puppetlabs.com/puppetlabs-release-${version}.deb && sudo dpkg -i puppetlabs-release-$version.deb && sudo apt-get update && sudo apt-get install -y puppet"
elif [[ $os = "CentOS" ]]
then
    is_puppet_installed="rpm -qa | grep -c puppet"
    install_puppet="sudo rpm -ivh --force http://yum.puppetlabs.com/puppetlabs-release-el-`cat /etc/centos-release | cut -d" " -f3 | cut -d . -f 1`.noarch.rpm && sudo yum install puppet"
else
    exit 1
fi

puppet_major_version=0
if [ `eval $is_puppet_installed` -ge 1 ]
then
    puppet_major_version=`puppet --version | cut -d . -f 1`
fi
if [ $puppet_major_version -eq 3 ]
then
    exit 0
fi

eval $install_puppet
